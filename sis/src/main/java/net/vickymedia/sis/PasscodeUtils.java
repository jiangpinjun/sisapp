package net.vickymedia.sis;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;


/**
 * Created by Ralph Jiang on 14-2-25.
 */
public class PasscodeUtils {

    public static interface OnPinLockInsertedListener{
        boolean onPinLockInserted(String passcode);
        void onPasscodeVerified(String pincode);
    }

    public static void initialLockView(Context context,View passcodeView,OnPinLockInsertedListener listener){

        passcodeView.setVisibility(View.VISIBLE);
        InputFilter[] filters = new InputFilter[2];
        filters[0]= new InputFilter.LengthFilter(1);
        filters[1] = new OnlyNumberInputFilter();

        View appUnlockLinearLayout1 = passcodeView.findViewById(R.id.AppUnlockLinearLayout1);

        PinCodeFieldTouchListener otl = new PinCodeFieldTouchListener();

        EditText pinCodeField1;EditText pinCodeField2; EditText pinCodeField3;EditText pinCodeField4;

        //Setup the pin fields row
        pinCodeField1 = (EditText) passcodeView.findViewById(R.id.pincode_1);
        setupPinItem(pinCodeField1,filters,otl);

        pinCodeField2 = (EditText) passcodeView.findViewById(R.id.pincode_2);
        setupPinItem(pinCodeField2,filters,otl);

        pinCodeField3 = (EditText) passcodeView.findViewById(R.id.pincode_3);
        setupPinItem(pinCodeField3,filters,otl);

        pinCodeField4 = (EditText) passcodeView.findViewById(R.id.pincode_4);
        setupPinItem(pinCodeField4,filters,otl);

        NumberButtonClickListener numberButtonClickListener = new NumberButtonClickListener(context,appUnlockLinearLayout1,pinCodeField1, pinCodeField2, pinCodeField3, pinCodeField4,listener);

        //setup the keyboard
        Button b = (Button) passcodeView.findViewById(R.id.button0);
        b.setTag("b-0");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button1);
        b.setTag("b-1");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button2);
        b.setTag("b-2");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button3);
        b.setTag("b-3");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button4);
        b.setTag("b-4");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button5);
        b.setTag("b-5");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button6);
        b.setTag("b-6");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button7);
        b.setTag("b-7");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button8);
        b.setTag("b-8");
        b.setOnClickListener(numberButtonClickListener);
        b = (Button) passcodeView.findViewById(R.id.button9);
        b.setTag("b-9");
        b.setOnClickListener(numberButtonClickListener);

        passcodeView.findViewById(R.id.button_erase).setOnClickListener(new EraseButtonClickListener(pinCodeField1, pinCodeField2, pinCodeField3, pinCodeField4));
    }

    protected static void setupPinItem(EditText item,InputFilter[] filters,View.OnTouchListener otl){
        item.setInputType(InputType.TYPE_NULL);
        item.setFilters(filters);
        item.setOnTouchListener(otl);
        item.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

    public static void showPasswordError(Context context){
        Toast toast = Toast.makeText(context, context.getString(R.string.passcode_wrong_passcode), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 30);
        toast.show();
    }

    public static class NumberButtonClickListener implements View.OnClickListener{

        EditText pinCodeField1;EditText pinCodeField2; EditText pinCodeField3;EditText pinCodeField4;
        View appUnlockLinearLayout1;
        OnPinLockInsertedListener listener;
        Context context;
        public NumberButtonClickListener(Context context,View appUnlockLinearLayout1,EditText pinCodeField1,EditText pinCodeField2,EditText pinCodeField3,EditText pinCodeField4,OnPinLockInsertedListener listener){
            this.pinCodeField1 = pinCodeField1;
            this.pinCodeField2 = pinCodeField2;
            this.pinCodeField3 = pinCodeField3;
            this.pinCodeField4 = pinCodeField4;
            this.listener = listener;
            this.context = context;
            this.appUnlockLinearLayout1 = appUnlockLinearLayout1;
        }


        @Override
        public void onClick(View view) {
            Object tag = view.getTag();
            if(null == tag || !(tag instanceof String) || !tag.toString().startsWith("b-")){
                return;
            }
            //set the value and move the focus
            String currentValueString = tag.toString().substring(2);

            boolean executed = false;

            if( pinCodeField1.isFocused()) {
                pinCodeField1.setText(currentValueString);
                pinCodeField2.requestFocus();
                pinCodeField2.setText("");
                executed = true;
            }
            else if( pinCodeField2.isFocused()) {
                pinCodeField2.setText(currentValueString);
                pinCodeField3.requestFocus();
                pinCodeField3.setText("");
                executed = true;
            }
            else if( pinCodeField3.isFocused()) {
                pinCodeField3.setText(currentValueString);
                pinCodeField4.requestFocus();
                pinCodeField4.setText("");
                executed = true;
            }
            else if( pinCodeField4.isFocused()) {
                pinCodeField4.setText(currentValueString);
                executed = true;
            }

            if(!executed){
                if( pinCodeField1.getText().length() == 0) {
                    pinCodeField1.setText(currentValueString);
                }
                else if( pinCodeField2.getText().length() == 0) {
                    pinCodeField2.setText(currentValueString);
                }
                else if( pinCodeField3.getText().length() == 0) {
                    pinCodeField3.setText(currentValueString);
                }
                else if(pinCodeField4.getText().length() == 0){
                    pinCodeField4.setText(currentValueString);
                }
            }

            if(pinCodeField4.getText().toString().length() > 0 &&
                    pinCodeField3.getText().toString().length() > 0 &&
                    pinCodeField2.getText().toString().length() > 0 &&
                    pinCodeField1.getText().toString().length() > 0
                    ) {

                String pincode = pinCodeField1.getText()+pinCodeField2.getText().toString()+pinCodeField3.getText().toString()+pinCodeField4.getText().toString();

                boolean success = listener.onPinLockInserted(pincode);

                if(success){
                    listener.onPasscodeVerified(pincode);
                }else{
                    Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
                    appUnlockLinearLayout1.startAnimation(shake);
                    showPasswordError(context);
                    pinCodeField1.setText("");
                    pinCodeField2.setText("");
                    pinCodeField3.setText("");
                    pinCodeField4.setText("");
                    pinCodeField1.requestFocus();
                }

            }
        }
    }

    public static class EraseButtonClickListener implements View.OnClickListener{

        EditText pinCodeField1;EditText pinCodeField2; EditText pinCodeField3;EditText pinCodeField4;

        public EraseButtonClickListener(EditText pinCodeField1,EditText pinCodeField2,EditText pinCodeField3,EditText pinCodeField4){
            this.pinCodeField1 = pinCodeField1;
            this.pinCodeField2 = pinCodeField2;
            this.pinCodeField3 = pinCodeField3;
            this.pinCodeField4 = pinCodeField4;
        }

        @Override
        public void onClick(View view) {
            boolean executed = false;
            if( pinCodeField4.isFocused() ) {
                pinCodeField3.requestFocus();
                pinCodeField3.setText("");
                executed = true;
            }
            else if( pinCodeField3.isFocused() ) {
                pinCodeField2.requestFocus();
                pinCodeField2.setText("");
                executed = true;
            }
            else if( pinCodeField2.isFocused() ) {
                pinCodeField1.requestFocus();
                pinCodeField1.setText("");
                executed = true;
            }else if(pinCodeField1.isFocused()){
                pinCodeField1.setText("");
                executed = true;
            }

            if(!executed){//TV,no no touch
                if( pinCodeField4.getText().length() > 0) {
                    pinCodeField4.setText("");
                }
                else if( pinCodeField3.getText().length() > 0) {
                    pinCodeField3.setText("");
                }
                else if( pinCodeField2.getText().length() > 0) {
                    pinCodeField2.setText("");
                }else if(pinCodeField1.getText().length() > 0){
                    pinCodeField1.setText("");
                }
            }

        }
    }


    public static class OnlyNumberInputFilter implements InputFilter{

        int length = 1;

        public OnlyNumberInputFilter(){
            this(1);
        }

        public OnlyNumberInputFilter(int length){
            this.length = length;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if( source.length() > length )
                return "";

            if( source.length() == 0 ) //erase
                return null;

            try {
                int number = Integer.parseInt(source.toString());
                if( ( number >= 0 ) && ( number <= 9 ) )
                    return String.valueOf(number);
                else
                    return source;
            } catch (NumberFormatException e) {
                return "";
            }
        }
    }

    public static class PinCodeFieldTouchListener implements View.OnTouchListener{

        @Override
        public boolean onTouch(View v, MotionEvent motionEvent) {
            if( v instanceof EditText ) {
                ((EditText)v).setText("");
            }
            return false;
        }
    }

    public static boolean isPasscodeEnabled(Context context){
        SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
        return proxyPrefs.getBoolean("LOCK.ENABLED",false);
    }

    public static boolean isPrivateBrowser(Context context){
        SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
        return proxyPrefs.getBoolean("LOCK.PRIVATEBROWSER",false);
    }

    public static String getStoredPasscode(Context context){
        SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
        return proxyPrefs.getString("LOCK.PASSCODE","");
    }

    public static void savePasscode(Context context,boolean enabledPasscode,String passcode,boolean privateBrowser){
        SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
        SharedPreferences.Editor prefsWriter = proxyPrefs.edit();
        prefsWriter.putBoolean("LOCK.ENABLED",enabledPasscode);
        prefsWriter.putString("LOCK.PASSCODE",passcode);
        prefsWriter.putBoolean("LOCK.PRIVATEBROWSER",privateBrowser);
        prefsWriter.commit();
    }

    public static void showPasscodeSettingDialog(Context context){
        Dialog dialog = new Dialog(context);
        dialog.setTitle(R.string.passcode_manage);
        dialog.setContentView(R.layout.passcode_setting);
        Button cancelBt = (Button)dialog.findViewById(R.id.passcode_cancel_bt);
        Button okBt = (Button)dialog.findViewById(R.id.passcode_ok_bt);
        cancelBt.setOnClickListener(new CancelBtClickListener(context,dialog));
        okBt.setOnClickListener(new OkBtClickListener(context,dialog));
        ToggleButton enable_passcode_txt = (ToggleButton) dialog.findViewById(R.id.enable_passcode_txt);
        ToggleButton enable_private_browser_txt = (ToggleButton) dialog.findViewById(R.id.enable_private_browser_txt);
        EditText passcode_txt = (EditText)dialog.findViewById(R.id.passcode_txt);
        InputFilter[] filters = new InputFilter[2];
        filters[0]= new InputFilter.LengthFilter(4);
        filters[1] = new OnlyNumberInputFilter(4);
        passcode_txt.setInputType(InputType.TYPE_CLASS_NUMBER);
        passcode_txt.setFilters(filters);
        enable_passcode_txt.setChecked(isPasscodeEnabled(context));
        enable_private_browser_txt.setChecked(isPrivateBrowser(context));
        String passcode = getStoredPasscode(context);
        passcode_txt.setText(passcode);
        dialog.show();
    }

    private static class OkBtClickListener implements View.OnClickListener{
        Dialog dialog;
        Context context;
        public OkBtClickListener(Context context,Dialog dialog){
            this.dialog = dialog;
            this.context = context;
        }
        @Override
        public void onClick(View view) {
            ToggleButton enable_passcode_txt = (ToggleButton) dialog.findViewById(R.id.enable_passcode_txt);
            ToggleButton enable_private_browser_txt = (ToggleButton) dialog.findViewById(R.id.enable_private_browser_txt);
            EditText passcode_txt = (EditText)dialog.findViewById(R.id.passcode_txt);
            String passcode = passcode_txt.getText().toString().trim();
            if(enable_passcode_txt.isChecked()){
                if(passcode.length() != 4){
                    Toast toast = Toast.makeText(context, context.getString(R.string.passcode_length_error), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 30);
                    toast.show();
                    return;
                }
            }
            savePasscode(context,enable_passcode_txt.isChecked(),passcode,enable_private_browser_txt.isChecked());
            dialog.dismiss();
        }
    }

    private static class CancelBtClickListener implements View.OnClickListener{
        Dialog dialog;
        Context context;
        public CancelBtClickListener(Context context,Dialog dialog){
            this.dialog = dialog;
            this.context = context;
        }
        @Override
        public void onClick(View view) {
            dialog.cancel();
            dialog.dismiss();
        }
    }


}
