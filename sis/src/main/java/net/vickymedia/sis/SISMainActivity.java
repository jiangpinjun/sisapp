package net.vickymedia.sis;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import net.vickymedia.sis.adapter.ForumListAdapter;
import net.vickymedia.sis.model.ForumItem;
import net.vickymedia.sis.model.TopicItem;
import net.vickymedia.sis.network.SISNetwork;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ralph Jiang on 2/17/14.
 */

public class SISMainActivity extends AppCompatActivity {

    Handler myHandler = new Handler();
    private ProgressDialog dialog;
    private ForumListAdapter forumListAdapter;
    /**
     * 双击退出函数
     */
    private static Boolean isExit = false;
    private Thread topicFetchThread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sismain);
        ListView forumListView = (ListView) findViewById(R.id.forumListView);
        getResources().obtainTypedArray(R.array.forum_icons);
        forumListAdapter = ForumListAdapter.build(this, R.layout.forum_list_item, getResources().obtainTypedArray(R.array.forum_titles), getResources().obtainTypedArray(R.array.forum_icons), getResources().obtainTypedArray(R.array.forum_urls));
        forumListView.setAdapter(forumListAdapter);

        forumListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SISMainActivity.this.forumItemClick(adapterView, view, i, l);
            }
        });

        dialog = new ProgressDialog(this);
        dialog.setTitle(R.string.process_dialog_title);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                try{
                    if(null != topicFetchThread && topicFetchThread.isAlive()){
                        topicFetchThread.interrupt();
                    }
                }catch (Exception e){

                }
                topicFetchThread = null;
            }
        });

        if(PasscodeUtils.isPasscodeEnabled(this)){
            PasscodeUtils.initialLockView(this, findViewById(R.id.passcode_scroll), new MyOnPinLockInsertedListener());
        }else{
            findViewById(R.id.passcode_scroll).setVisibility(View.GONE);
        }
    }

    private class MyOnPinLockInsertedListener implements PasscodeUtils.OnPinLockInsertedListener{

        @Override
        public boolean onPinLockInserted(String passcode) {
            return passcode.equals(PasscodeUtils.getStoredPasscode(SISMainActivity.this));
        }

        @Override
        public void onPasscodeVerified(String passcode) {
            SISMainActivity.this.findViewById(R.id.passcode_scroll).setVisibility(View.GONE);
            View focusView = getCurrentFocus();
            if(null != focusView){
                focusView.postInvalidate();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            exitBy2Click();      //调用双击退出函数
        }
        return false;
    }


    private void exitBy2Click() {
        Timer tExit = null;
        if (!isExit) {
            isExit = true; // 准备退出
            Toast.makeText(this, R.string.click_again_exist_msg, Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false; // 取消退出
                }
            }, 2500); // 如果2秒钟内没有按下返回键，则启动定时器取消掉刚才执行的任务

        } else {
            if(PasscodeUtils.isPrivateBrowser(this)){
                ImageLoader.getInstance().clearMemoryCache();
                ImageLoader.getInstance().clearDiscCache();
            }
            finish();
            System.exit(0);
        }
    }

    private void forumItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        ListView forumListView = (ListView) adapterView;
        ForumItem item = (ForumItem) forumListView.getAdapter().getItem(position);
        if (item.getTopics() == null || item.getTopics().isEmpty()) {
            topicFetchThread = new Thread(new TopicFetchRunnable(item,this));
            topicFetchThread.start();
            dialog.setMessage(String.format(getString(R.string.topic_fetch_message),item.getTitle()));
            dialog.show();
        } else {
            Intent intent = new Intent(this, TopicActivity.class);
            intent.putExtra(Constants.FORUM_ITEM_KEY, item);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sismain, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(findViewById(R.id.passcode_scroll).getVisibility() == View.VISIBLE){
            return true;
        }
        switch (item.getItemId()) {
            case R.id.item_clear_cache:
                ImageLoader.getInstance().clearMemoryCache();
                ImageLoader.getInstance().clearDiscCache();
                clearForumTopics();
                Toast toast = Toast.makeText(SISMainActivity.this,R.string.menu_item_clear_cache_msg,Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return true;
            /*case R.id.item_proxy_setting:
                ProxySettingUtils.showProxySettingDialog(this);
                return true;*/
            case R.id.item_check_sis_host:
                dialog.setMessage(getString(R.string.checking_sis_host_msg));
                new Thread(new CheckingSISHostRunnable(this)).start();
                dialog.show();
                return true;
            case R.id.item_passcode_setting:
                PasscodeUtils.showPasscodeSettingDialog(this);
                return true;
            default:
                return false;
        }
    }



    protected void clearForumTopics(){
        int count = forumListAdapter.getCount();
        for(int i=0;i<count;i++){
            forumListAdapter.getItem(i).clearTopics();
        }
    }

    public void topicListFetched(final ForumItem forumItem){
        myHandler.post(new Runnable() {
            @Override
            public void run() {
                if(null != topicFetchThread){
                    topicFetchThread = null;
                    dialog.dismiss();
                    Intent intent = new Intent(SISMainActivity.this, TopicActivity.class);
                    intent.putExtra(Constants.FORUM_ITEM_KEY, forumItem);
                    startActivity(intent);
                }
            }
        });
    }

    public void topicListFetchedFailed(final ForumItem forumItem,final Exception e){
        final String errorInfoTitle = getString(R.string.topic_fetch_error, forumItem.getTitle());
        myHandler.post(new Runnable() {
            @Override
            public void run() {
                if(null != topicFetchThread){
                    topicFetchThread = null;
                    dialog.dismiss();
                    Toast toast = Toast.makeText(SISMainActivity.this,errorInfoTitle,Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                Log.e(Constants.SIS_LOG_TAG,"topicListFetchedFailed",e);
            }
        });
    }

    public void sisHostCheckFinished(final String sisUrl){
        myHandler.post(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                String errorInfoTitle = null;
                if(sisUrl == null){
                    errorInfoTitle = getString(R.string.check_sis_host_error_msg);
                }else{
                    errorInfoTitle = getString(R.string.check_sis_host_success_msg);
                }
                Toast toast = Toast.makeText(SISMainActivity.this,errorInfoTitle,Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });
    }

    static class CheckingSISHostRunnable implements Runnable{
        SISMainActivity activity;
        public CheckingSISHostRunnable(SISMainActivity activity){
            this.activity = activity;
        }

        @Override
        public void run() {
            String url = SISNetwork.checkAvailableSISUrl(activity);
            activity.sisHostCheckFinished(url);
        }
    }

    static class TopicFetchRunnable implements Runnable{

        ForumItem forumItem;
        SISMainActivity activity;

        public TopicFetchRunnable(ForumItem forumItem,SISMainActivity activity){
            this.forumItem = forumItem;
            this.activity = activity;
        }
        @Override
        public void run() {
            try{
                List<TopicItem> topicItemList = SISNetwork.fetchTopic(forumItem.getUrl(),forumItem.getPage()+1);
                forumItem.setTopics(topicItemList);
                //forumItem.setPage(forumItem.getPage()+1);
                activity.topicListFetched(forumItem);

            }catch (Exception e){
                activity.topicListFetchedFailed(forumItem,e);
            }
        }
    }
}
