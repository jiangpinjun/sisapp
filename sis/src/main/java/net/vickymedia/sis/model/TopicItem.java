package net.vickymedia.sis.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ralph.jiang on 2/17/14.
 */
public class TopicItem implements Serializable {

    private static final long serialVersionUID = 2184536328769695905L;

    private String title;
    private String url;
    private String[] images;

    public TopicItem(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public void setImages(List<String> images) {
        if (null == images) {
            this.images = null;
        } else {
            this.images = images.toArray(new String[0]);
        }
    }

    @Override
    public String toString() {
        return "TopicItem{" +
                "title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", images=" + Arrays.toString(images) +
                '}';
    }
}
