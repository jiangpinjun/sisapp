package net.vickymedia.sis.model;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * Created by Ralph Jiang on 14-2-22.
 */
public class ProxySetting implements Serializable {

    private static final long serialVersionUID = 9078247759405418399L;
    private static final ProxySetting INSTANCE = new ProxySetting();
    private boolean proxyEnabled;
    /**
     * 0:basic, 1 ntlm
     */
    private int proxyType;
    private String proxyHost;
    private String proxyPort;
    private String proxyUser;
    private String proxyPass;

    public boolean isProxyEnabled() {
        return proxyEnabled;
    }

    public void setProxyEnabled(boolean proxyEnabled) {
        this.proxyEnabled = proxyEnabled;
    }

    public int getProxyType() {
        return proxyType;
    }

    public void setProxyType(int proxyType) {
        this.proxyType = proxyType;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyUser() {
        return proxyUser;
    }

    public void setProxyUser(String proxyUser) {
        this.proxyUser = proxyUser;
    }

    public String getProxyPass() {
        return proxyPass;
    }

    public void setProxyPass(String proxyPass) {
        this.proxyPass = proxyPass;
    }

    public void settingByOther(ProxySetting proxySetting){
        this.proxyEnabled = proxySetting.proxyEnabled;
        this.proxyType = proxySetting.proxyType;
        this.proxyHost = proxySetting.proxyHost;
        this.proxyPort = proxySetting.proxyPort;
        this.proxyUser = proxySetting.proxyUser;
        this.proxyPass = proxySetting.proxyPass;
    }

    public String buildProxyString() {
        if (!proxyEnabled) {
            return null;
        }
        if (null == proxyHost || proxyHost.trim().length() == 0 || null == proxyPort || proxyPort.trim().length() == 0) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        if (proxyType == 1) {//ntlm
            builder.append("ntlm:");
        }

        builder.append(proxyHost.trim()).append(':').append(proxyPort.trim());
        if (null != proxyUser && proxyUser.trim().length() > 0 && null != proxyPass && proxyPass.trim().length() > 0) {
            builder.append(':');
            int index = proxyUser.indexOf('\\');
            if(index > 0){
                builder.append(proxyUser.substring(index+1)).append(':').append(proxyPass.trim()).append(':').append(proxyUser.substring(0,index));
            }else{
                builder.append(proxyUser.trim()).append(':').append(proxyPass.trim());
            }
        }else if(proxyType == 1){
            return null;
        }

        return builder.toString();
    }

    @Override
    public String toString() {
        return "ProxySetting{" +
                "proxyEnabled=" + proxyEnabled +
                ", proxyType=" + proxyType +
                ", proxyHost='" + proxyHost + '\'' +
                ", proxyPort='" + proxyPort + '\'' +
                ", proxyUser='" + proxyUser + '\'' +
                ", proxyPass='" + proxyPass + '\'' +
                '}';
    }
}
