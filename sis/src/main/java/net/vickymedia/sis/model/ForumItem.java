package net.vickymedia.sis.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ralph.jiang on 2/17/14.
 */
public class ForumItem implements Serializable {

    private static final long serialVersionUID = -5965491510953603219L;

    private String title;
    private int icon;
    private String url;
    private int page = 0;
    private List<TopicItem> topics;

    public ForumItem(String title, int icon) {
        this.setTitle(title);
        this.setIcon(icon);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<TopicItem> getTopics() {
        if(topics == null){
            topics = new ArrayList<TopicItem>();
        }
        return topics;
    }

    public void setTopics(List<TopicItem> topics) {
        if(null == topics){
            return;
        }
        getTopics().clear();
        getTopics().addAll(topics);
        page = 1;
    }

    public void addTopics(List<TopicItem> topics) {
        if (null == topics || topics.isEmpty()) {
            return;
        }
        List<TopicItem> holdTopics = getTopics();
        holdTopics.addAll(topics);
    }

    public void clearTopics() {
        if (null == topics) {
            return;
        }
        topics.clear();
        topics = null;
        page = 0;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "ForumItem{" +
                "title='" + title + '\'' +
                ", icon=" + icon +
                ", url='" + url + '\'' +
                ", page=" + page +
                ", topics=" + topics +
                '}';
    }
}
