package net.vickymedia.sis;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import net.vickymedia.sis.component.progressbar.CircleProgressBar;
import net.vickymedia.sis.model.TopicItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import uk.co.senab.photoview.PhotoView;

import static android.view.animation.Animation.AnimationListener;
import static net.vickymedia.sis.Constants.TOPIC_ITEM_KEY;
import static uk.co.senab.photoview.PhotoViewAttacher.OnViewTapListener;

/**
 * Created by Ralph Jiang on 2/17/14.
 */

public class ImagePagerActivity extends Activity implements AnimationListener, OnViewTapListener {

    DisplayImageOptions options;
    ViewPager pager;
    View image_float_controls;
    TextView imageTopicTitle;
    Animation animFadeIn, animFadeOut;
    TopicItem topicItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_image_pager);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        topicItem = (TopicItem) bundle.getSerializable(TOPIC_ITEM_KEY);
        assert topicItem != null;
        //setTitle(topicItem.getTitle());

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .showImageOnLoading(R.drawable.alpha_image)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.NONE)
                //.bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                //.displayer(new SimpleBitmapDisplayer())
                .build();

        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_out);

        animFadeOut.setAnimationListener(this);
        animFadeIn.setAnimationListener(this);
        image_float_controls = findViewById(R.id.image_float_controls);
        imageTopicTitle = (TextView) findViewById(R.id.imageTopicTitle);
        imageTopicTitle.setText(String.format("%s - (1 / %d)", topicItem.getTitle(), topicItem.getImages().length));
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                imageTopicTitle.setText(String.format("%s - (%d / %d)", topicItem.getTitle(), position + 1, topicItem.getImages().length));
            }
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setAdapter(new ImagePagerAdapter(topicItem.getImages()));
        pager.setCurrentItem(0);
    }

    public void doneButtonClicked(View view) {
        finish();
        //ImageLoader.getInstance().stop();
    }

    public void saveImageButtonClicked(View view){
        int currentPage = pager.getCurrentItem();
        PhotoView v = (PhotoView)pager.findViewWithTag("v-"+currentPage);
        if(v == null || v.getDrawable() == null){
            //TODO
            return;
        }

        String[] imageUrls = topicItem.getImages();
        String imageUrl = imageUrls[currentPage];
        int saved = saveBitmapToAlbum(this,getRequestedOrientation(),imageUrl,((BitmapDrawable)v.getDrawable()).getBitmap(),topicItem.getTitle(),"");
        String msg = null;
        if(saved == 0){
            msg = getString(R.string.save_photo_success_msg);
        }else{
            int msgId = 0;
            switch (saved){
                case 1:
                    msgId = R.string.save_photo_error_nosd_msg;
                    break;
                case 2:
                    msgId = R.string.save_photo_error_fold_msg;
                    break;
                default:
                    msgId = R.string.save_photo_error_msg;
                    break;
            }
            msg = getString(msgId);
        }
        Toast t = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
    }

    public static int saveBitmapToAlbum(Context context,int orientation,String imageUrl,Bitmap bitmap,String title,String description){

        ContentResolver contentResolver = context.getContentResolver();

        if(!Environment.getExternalStorageDirectory().exists()){
            return 1;
        }

        File sdCardFolder = new File(Environment.getExternalStorageDirectory(),Constants.SIS_PHOTO_FOLDER);
        if(!sdCardFolder.exists()){
            if(!sdCardFolder.mkdirs()){
                return 2;
            }
        }
        File photoFile = new File(sdCardFolder,imageUrl.hashCode()+".jpg");
        OutputStream fOut = null;
        try {
            fOut = new FileOutputStream(photoFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,fOut);
        }catch (IOException e){
            if(null != fOut){
                try {
                    fOut.close();
                    fOut = null;
                } catch (IOException e1) {

                }
                photoFile.delete();
            }
            Log.e(Constants.SIS_LOG_TAG,"Write photo error",e);
            return 3;
        }finally {
            if(null != fOut){
                try {
                    fOut.close();
                } catch (IOException e) {

                }
            }
        }

        ContentValues values = new ContentValues(6);
        values.put(MediaStore.Images.Media.TITLE, title);
        values.put(MediaStore.Images.Media.DESCRIPTION, description);
        values.put(MediaStore.Images.Media.DATE_TAKEN, imageUrl.hashCode());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.Images.Media.ORIENTATION, 0);
        values.put(MediaStore.Images.Media.WIDTH,bitmap.getWidth());
        values.put(MediaStore.Images.Media.HEIGHT,bitmap.getHeight());
        values.put(MediaStore.Images.Media.DATA, photoFile.getAbsolutePath());

        //values.put(MediaStore.Images.Media.SIZE, size);
        Uri url = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        if(null == url){
            int rows = contentResolver.update(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values,MediaStore.Images.Media.DATE_TAKEN+"='"+imageUrl.hashCode()+"'",null);
            if(Log.isLoggable(Constants.SIS_LOG_TAG,Log.DEBUG)){
                Log.d(Constants.SIS_LOG_TAG,"update photo result = " + rows);
            }
            return rows > 0 ? 0 : 4;
        }
        if(Log.isLoggable(Constants.SIS_LOG_TAG,Log.DEBUG)){
            Log.d(Constants.SIS_LOG_TAG,url.toString());
        }
        return 0;

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if(image_float_controls.getVisibility() == View.VISIBLE){
            animFadeOut.setStartOffset(3000);
            image_float_controls.startAnimation(animFadeOut);
        }
    }

    protected void changeImageFloatControls() {
        if (image_float_controls.getVisibility() == View.GONE) {
            image_float_controls.setVisibility(View.VISIBLE);
            image_float_controls.startAnimation(animFadeIn);
        } else {
            image_float_controls.startAnimation(animFadeOut);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == animFadeOut) {
            animFadeOut.setStartOffset(0);
            image_float_controls.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onViewTap(View view, float x, float y) {
        changeImageFloatControls();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
            changeImageFloatControls();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    private class ImagePagerAdapter extends PagerAdapter {

        private String[] images;
        private LayoutInflater inflater;

        ImagePagerAdapter(String[] images) {
            this.images = images;
            inflater = getLayoutInflater();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return images == null ? 0 : images.length;
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
            assert imageLayout != null;
            PhotoView imageView = (PhotoView) imageLayout.findViewById(R.id.image);
            imageView.setTag("v-"+position);
            imageView.setOnViewTapListener(ImagePagerActivity.this);
            CircleProgressBar spinner = (CircleProgressBar) imageLayout.findViewById(R.id.loading);
            SISImageLoadingListener listener = new SISImageLoadingListener(ImagePagerActivity.this,spinner);

            ImageLoader.getInstance().displayImage(images[position], imageView, options, listener, listener);

            view.addView(imageLayout, 0);
            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }

    private static class SISImageLoadingListener implements ImageLoadingListener,ImageLoadingProgressListener{

        Context context;
        CircleProgressBar spinner;

        public SISImageLoadingListener(Context context,CircleProgressBar spinner){
            this.spinner = spinner;
            this.context = context;
            this.spinner.setProgress(0);
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            int message = 0;
            switch (failReason.getType()) {
                case IO_ERROR:
                    message = R.string.image_load_io_error;
                    break;
                case DECODING_ERROR:
                    message = R.string.image_load_decode_error;
                    break;
                case NETWORK_DENIED:
                    message = R.string.image_load_denied_error;
                    break;
                case OUT_OF_MEMORY:
                    message = R.string.image_load_oom_error;
                    break;
                case UNKNOWN:
                    message = R.string.image_load_unknown_error;
                    break;
            }
            spinner.setVisibility(View.GONE);
            Toast t = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            //spinner.setProgress(spinner.getMax());
            spinner.setVisibility(View.GONE);
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {

        }

        @Override
        public void onProgressUpdate(String imageUri, View view, int current, int total) {
            //Log.i(Constants.SIS_LOG_TAG, String.format("%s -> %d - %d",imageUri, current, total));
            if(total <= 0){
                return;
            }
            if(spinner.getMax() != total){
                spinner.setMax(total);
            }
            spinner.setProgress(current);
        }
    }
}
