package net.vickymedia.sis;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import net.vickymedia.net.DefaultConnectService;
import net.vickymedia.sis.network.SISImageDownloader;
import net.vickymedia.sis.network.SISNetwork;

/**
 * Created by Ralph Jiang on 2/17/14.
 */
public class SISApplication extends Application {

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressWarnings("unused")
    @Override
    public void onCreate() {
        if (Constants.Config.DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
        }
        super.onCreate();
        initImageLoader(getApplicationContext());
        initConnectService(getApplicationContext());
        initSISUrls(getApplicationContext());
    }

    public static void initSISUrls(Context context) {
        Constants.setSISHosts(context.getResources().getStringArray(R.array.sis_hosts));
        SISNetwork.recoverSISUrl(context);
    }

    public static void initConnectService(Context context) {
        DefaultConnectService defaultConnectService = DefaultConnectService.getInstance();
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        defaultConnectService.setCookieStore(cookieJar);
        //defaultConnectService.setHttpProxy("172.18.49.228:80");
        ProxySettingUtils.initProxySetting(context);
        if(ProxySettingUtils.getProxySetting().isProxyEnabled()){
            defaultConnectService.setHttpProxy(ProxySettingUtils.getProxySetting().buildProxyString());
        }
    }

    @Override
    public void onLowMemory() {
        ImageLoader.getInstance().clearMemoryCache();
        super.onLowMemory();
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator()).imageDownloader(new SISImageDownloader(context, DefaultConnectService.getInstance()))
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }
}
