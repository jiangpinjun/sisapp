package net.vickymedia.sis;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.vickymedia.sis.adapter.TopicListAdapter;
import net.vickymedia.sis.model.ForumItem;
import net.vickymedia.sis.model.TopicItem;
import net.vickymedia.sis.network.SISNetwork;

import java.util.LinkedList;
import java.util.List;
/**
 * Created by Ralph Jiang on 2/17/14.
 */
public class TopicActivity extends AppCompatActivity {
    private ForumItem forumItem = null;
    Handler myHandler = new Handler();
    private ProgressDialog dialog;
    private PullToRefreshListView mPullRefreshListView;
    private TopicListAdapter adapter;

    Thread imageFetchThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        forumItem = (ForumItem) bundle.getSerializable(Constants.FORUM_ITEM_KEY);
        assert forumItem != null;
        setTitle(forumItem.getTitle());
        mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.topicListView);
        mPullRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
        mPullRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>(){
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                new RefreshTopicListTask().execute(forumItem);
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                new LoadMoreTopicListTask().execute(forumItem);
            }
        });

        ListView topicListView = mPullRefreshListView.getRefreshableView();

        adapter = new TopicListAdapter(this, R.layout.topic_list_item, forumItem.getTopics());
        topicListView.setAdapter(adapter);
        topicListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TopicActivity.this.topicItemClick(adapterView, view, i, l);
            }
        });
        dialog = new ProgressDialog(this);
        dialog.setTitle(R.string.process_dialog_title);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                try{
                    if(null != imageFetchThread && imageFetchThread.isAlive()){
                        imageFetchThread.interrupt();
                    }
                }catch (Exception e){

                }
                imageFetchThread = null;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sismain, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_clear_cache:
                ImageLoader.getInstance().clearMemoryCache();
                ImageLoader.getInstance().clearDiscCache();
                Toast toast = Toast.makeText(TopicActivity.this,R.string.menu_item_clear_cache_msg,Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return true;
            /*
            case R.id.item_proxy_setting:
                ProxySettingUtils.showProxySettingDialog(this);
                return true;
                */
            case R.id.item_check_sis_host:
                dialog.setMessage(getString(R.string.checking_sis_host_msg));
                new Thread(new CheckingSISHostRunnable(this)).start();
                dialog.show();
                return true;
            case R.id.item_passcode_setting:
                PasscodeUtils.showPasscodeSettingDialog(this);
                return true;
            default:
                return false;
        }
    }

    private void topicItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        ListView topicListView = (ListView) adapterView;
        TopicItem item = (TopicItem) topicListView.getAdapter().getItem(position);
        if (item.getImages() == null || item.getImages().length == 0) {
            imageFetchThread = new Thread(new TopicImageFetchRunnable(item,this));
            imageFetchThread.start();
            dialog.setMessage(getString(R.string.topic_fetch_image_message));
            dialog.show();
        } else {
            Intent intent = new Intent(this, ImagePagerActivity.class);
            intent.putExtra(Constants.TOPIC_ITEM_KEY, item);
            startActivity(intent);
        }
    }

    public void topicListFetchedFailed(final ForumItem forumItem,final Exception e){
        final String errorInfoTitle = getString(R.string.topic_fetch_error, forumItem.getTitle());
        myHandler.post(new Runnable() {
            @Override
            public void run() {
                if(null != imageFetchThread){
                    imageFetchThread = null;
                    Toast toast = Toast.makeText(TopicActivity.this,errorInfoTitle,Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                Log.e(Constants.SIS_LOG_TAG,"topicListFetchedFailed",e);
            }
        });
    }
    public void topicImagesFetched(final TopicItem topicItem){
        myHandler.post(new Runnable() {
            @Override
            public void run() {
                if(imageFetchThread != null){
                    imageFetchThread = null;
                    dialog.dismiss();
                    Intent intent = new Intent(TopicActivity.this, ImagePagerActivity.class);
                    intent.putExtra(Constants.TOPIC_ITEM_KEY, topicItem);
                    startActivity(intent);
                }
            }
        });
    }
    public void topicImagesFetchedFailed(final TopicItem topicItem,final Exception e){
        final String errorInfoTitle = getString(R.string.topic_fetch_image_error, topicItem.getTitle());
        myHandler.post(new Runnable() {
            @Override
            public void run() {
                if(imageFetchThread != null){
                    imageFetchThread = null;
                    dialog.dismiss();
                    Toast toast = Toast.makeText(TopicActivity.this,errorInfoTitle,Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                if(null != e){
                    Log.e(Constants.SIS_LOG_TAG, "topicImagesFetchedFailed", e);
                }else{
                    Log.e(Constants.SIS_LOG_TAG, "topicImagesFetchedFailed no images");
                }
            }
        });
    }

    public void sisHostCheckFinished(final String sisUrl){
        myHandler.post(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                String errorInfoTitle = null;
                if(sisUrl == null){
                    errorInfoTitle = getString(R.string.check_sis_host_error_msg);
                }else{
                    errorInfoTitle = getString(R.string.check_sis_host_success_msg);
                }
                Toast toast = Toast.makeText(TopicActivity.this,errorInfoTitle,Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });
    }

    static class CheckingSISHostRunnable implements Runnable{
        TopicActivity activity;
        public CheckingSISHostRunnable(TopicActivity activity){
            this.activity = activity;
        }

        @Override
        public void run() {
            String url = SISNetwork.checkAvailableSISUrl(activity);
            activity.sisHostCheckFinished(url);
        }
    }

    static class TopicImageFetchRunnable implements Runnable{
        TopicItem topicItem;
        TopicActivity activity;

        public TopicImageFetchRunnable(TopicItem topicItem,TopicActivity activity){
            this.topicItem = topicItem;
            this.activity = activity;
        }
        @Override
        public void run() {
            try{
                String[] images = SISNetwork.fetchTopicImages(topicItem.getUrl());
                if(null == images || images.length == 0){
                    activity.topicImagesFetchedFailed(topicItem, null);
                }else{
                    topicItem.setImages(images);
                    activity.topicImagesFetched(topicItem);
                }
            }catch (Exception e){
                activity.topicImagesFetchedFailed(topicItem, e);
            }
        }
    }

    private class LoadMoreTopicListTask extends AsyncTask<ForumItem,Integer,List<List<TopicItem>>>{
        @Override
        protected List<List<TopicItem>> doInBackground(ForumItem... forumItems) {
            LinkedList<List<TopicItem>> result = new LinkedList<List<TopicItem>>();
            for(int i=0;i<forumItems.length;i++){
                ForumItem item = forumItems[i];
                try{
                    List<TopicItem> topicItemList = SISNetwork.fetchTopic(item.getUrl(),item.getPage()+1);
                    item.addTopics(topicItemList);
                    if(null != topicItemList){
                        item.setPage(item.getPage()+1);
                        result.add(topicItemList);
                    }
                }catch (Exception e){
                    topicListFetchedFailed(item,e);
                }
                publishProgress((int) ((i / (float) forumItems.length) * 100));
                if (isCancelled()) break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<List<TopicItem>> topicItems) {
            if(!topicItems.isEmpty()){
                List<TopicItem> items = topicItems.get(0);
                for(TopicItem item : items){
                    adapter.add(item);
                }
            }
            mPullRefreshListView.onRefreshComplete();
            super.onPostExecute(topicItems);
        }
    }

    private class RefreshTopicListTask extends AsyncTask<ForumItem,Integer,List<List<TopicItem>>>{
        @Override
        protected List<List<TopicItem>> doInBackground(ForumItem... forumItems) {
            LinkedList<List<TopicItem>> result = new LinkedList<List<TopicItem>>();
            for(int i=0;i<forumItems.length;i++){
                ForumItem item = forumItems[i];
                try{
                    List<TopicItem> topicItemList = SISNetwork.fetchTopic(item.getUrl(),1);
                    item.setTopics(topicItemList);
                    if(null != topicItemList){
                        result.add(topicItemList);
                    }
                }catch (Exception e){
                    topicListFetchedFailed(item,e);
                }
                publishProgress((int) ((i / (float) forumItems.length) * 100));
                if (isCancelled()) break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<List<TopicItem>> topicItems) {
            if(!topicItems.isEmpty()){
                adapter.clear();
                List<TopicItem> items = topicItems.get(0);
                for(TopicItem item : items){
                    adapter.add(item);
                }
            }
            mPullRefreshListView.onRefreshComplete();
            super.onPostExecute(topicItems);
        }
    }

}
