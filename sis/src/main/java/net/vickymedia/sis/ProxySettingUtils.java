package net.vickymedia.sis;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import net.vickymedia.net.DefaultConnectService;
import net.vickymedia.sis.model.ProxySetting;

/**
 * Created by Ralph Jiang on 14-2-22.
 */
public class ProxySettingUtils {


    private static final ProxySetting PROXY_SETTING = new ProxySetting();

    public static ProxySetting getProxySetting(){
        return PROXY_SETTING;
    }

    public static ProxySetting initProxySetting(Context context) {
        SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
        PROXY_SETTING.setProxyHost(proxyPrefs.getString("proxyHost", System.getProperty("http.proxyHost")));
        PROXY_SETTING.setProxyPort(proxyPrefs.getString("proxyPort", System.getProperty("http.proxyPort")));
        PROXY_SETTING.setProxyUser(proxyPrefs.getString("proxyUser", null));
        PROXY_SETTING.setProxyPass(proxyPrefs.getString("proxyPass", null));
        PROXY_SETTING.setProxyType(proxyPrefs.getInt("proxyType", 0));
        PROXY_SETTING.setProxyEnabled(proxyPrefs.getBoolean("proxyEnabled", false));
        return PROXY_SETTING;
}
    
    public static void storeProxySetting(Context context){
        SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
        SharedPreferences.Editor prefsWriter = proxyPrefs.edit();
        prefsWriter.putBoolean("proxyEnabled",PROXY_SETTING.isProxyEnabled());
        prefsWriter.putInt("proxyType", PROXY_SETTING.getProxyType());
        if(null != PROXY_SETTING.getProxyHost()){
            prefsWriter.putString("proxyHost",PROXY_SETTING.getProxyHost());
        }
        if(null != PROXY_SETTING.getProxyPort()){
            prefsWriter.putString("proxyPort",PROXY_SETTING.getProxyPort());
        }
        if(null != PROXY_SETTING.getProxyUser()){
            prefsWriter.putString("proxyUser",PROXY_SETTING.getProxyUser());
        }
        if(null != PROXY_SETTING.getProxyPass()){
            prefsWriter.putString("proxyPass",PROXY_SETTING.getProxyPass());
        }
        prefsWriter.commit();
    }

    public static Dialog showProxySettingDialog(Context context){

        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.proxy_setting);
        Button cancelBt = (Button)dialog.findViewById(R.id.proxy_cancel_bt);
        Button okBt = (Button)dialog.findViewById(R.id.proxy_ok_bt);
        cancelBt.setOnClickListener(new CancelBtClickListener(context,dialog));
        okBt.setOnClickListener(new OkBtClickListener(context,dialog));
        ToggleButton enable_proxy_txt = (ToggleButton) dialog.findViewById(R.id.enable_proxy_txt);
        RadioButton proxy_type_basic_txt = (RadioButton)dialog.findViewById(R.id.proxy_type_basic_txt);
        RadioButton proxy_type_ntlm_txt = (RadioButton)dialog.findViewById(R.id.proxy_type_ntlm_txt);
        EditText proxy_server_txt = (EditText)dialog.findViewById(R.id.proxy_server_txt);
        EditText proxy_user_txt = (EditText)dialog.findViewById(R.id.proxy_user_txt);
        EditText proxy_password_txt = (EditText)dialog.findViewById(R.id.proxy_password_txt);
        enable_proxy_txt.setChecked(PROXY_SETTING.isProxyEnabled());
        if(PROXY_SETTING.getProxyType() == 1){
            proxy_type_ntlm_txt.setChecked(true);
        }else{
            proxy_type_basic_txt.setChecked(true);
        }
        if(null != PROXY_SETTING.getProxyHost() && null != PROXY_SETTING.getProxyPort() && PROXY_SETTING.getProxyHost().trim().length() > 0 && PROXY_SETTING.getProxyPort().length() > 0){
            proxy_server_txt.setText(PROXY_SETTING.getProxyHost().trim()+":"+PROXY_SETTING.getProxyPort());
        }
        if(null != PROXY_SETTING.getProxyUser()){
            proxy_user_txt.setText(PROXY_SETTING.getProxyUser());
        }
        if(null != PROXY_SETTING.getProxyPass()){
            proxy_password_txt.setText(PROXY_SETTING.getProxyPass());
        }
        dialog.setTitle(R.string.menu_item_proxy_setting);
        dialog.setCancelable(true);
        dialog.show();
        return dialog;
    } 
    private static class OkBtClickListener implements View.OnClickListener{

        Dialog dialog;
        Context context;

        public OkBtClickListener(Context context,Dialog dialog){
            this.dialog = dialog;
            this.context = context;
        }
        @Override
        public void onClick(View view) {
            ToggleButton enable_proxy_txt = (ToggleButton) dialog.findViewById(R.id.enable_proxy_txt);
            RadioButton proxy_type_basic_txt = (RadioButton)dialog.findViewById(R.id.proxy_type_basic_txt);
            EditText proxy_server_txt = (EditText)dialog.findViewById(R.id.proxy_server_txt);
            EditText proxy_user_txt = (EditText)dialog.findViewById(R.id.proxy_user_txt);
            EditText proxy_password_txt = (EditText)dialog.findViewById(R.id.proxy_password_txt);
            String server = proxy_server_txt.getText().toString().trim();
            String user = proxy_user_txt.getText().toString().trim();
            String pass = proxy_password_txt.getText().toString().trim();
            int resId = -1;
            if(enable_proxy_txt.isChecked()){
                if(server.length() == 0){
                    resId = R.string.proxy_server_empty_msg;
                }
                else if(server.indexOf(':') <= 0){
                    resId = R.string.proxy_server_noport_msg;
                }
                else if(!proxy_type_basic_txt.isChecked()){//ntlm
                    if(user.length() == 0){
                        resId = R.string.proxy_user_empty_msg;
                    }
                    else if(user.indexOf('\\') <= 0){
                        resId = R.string.proxy_user_nodomain_msg;
                    }
                    else if(pass.length() == 0){
                        resId = R.string.proxy_pass_empty_msg;
                    }
                }
            }
            if(resId != -1){
                Toast toast =  Toast.makeText(context,resId,Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                return;
            }

            PROXY_SETTING.setProxyEnabled(enable_proxy_txt.isChecked());
            PROXY_SETTING.setProxyType(proxy_type_basic_txt.isChecked() ? 0 : 1);
            if(server.length() > 0){
               int index = server.indexOf(':');
                if(index > 0){
                    PROXY_SETTING.setProxyHost(server.substring(0,index));
                    PROXY_SETTING.setProxyPort(server.substring(index+1));
                }
            }
            PROXY_SETTING.setProxyUser(user);
            PROXY_SETTING.setProxyPass(pass);

            if(PROXY_SETTING.isProxyEnabled()){
                DefaultConnectService.getInstance().setHttpProxy(PROXY_SETTING.buildProxyString());
            }else{
                DefaultConnectService.getInstance().removeProxy();
            }
            storeProxySetting(context);
            dialog.dismiss();
        }
    }

    private static class CancelBtClickListener implements View.OnClickListener{
        Dialog dialog;
        Context context;
        public CancelBtClickListener(Context context,Dialog dialog){
           this.dialog = dialog;
           this.context = context;
        }
        @Override
        public void onClick(View view) {
            dialog.cancel();
            dialog.dismiss();
        }
    }
}
