package net.vickymedia.sis.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import net.vickymedia.net.DefaultConnectService;
import net.vickymedia.sis.Constants;
import net.vickymedia.sis.model.TopicItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * Created by ralph.jiang on 2/17/14.
 */
public class SISNetwork {

    public static void login() throws Exception {
        if (isLogined()) {
            return;
        }
        String sisLoginURL = Constants.getSISLoginURL();
        if (Log.isLoggable(Constants.SIS_LOG_TAG, Log.DEBUG)) {
            Log.d(Constants.SIS_LOG_TAG, "login " + sisLoginURL);
        }
        Map<String, Object> loginParams = new HashMap<String, Object>();
        String logic_ok_tag = null;
        //for pc
        loginParams.put("formhash", "7b351e85");
        loginParams.put("referer", "index.php");
        loginParams.put("loginfield", "username");
        loginParams.put("username", Constants.SIS_USER);
        loginParams.put("password", Constants.SIS_PASSWORD);
        loginParams.put("questionid", "0");
        loginParams.put("answer", "");
        loginParams.put("cookietime", "315360000");
        loginParams.put("loginmode", "");
        loginParams.put("styleid", "");
        loginParams.put("loginsubmit", "true");
        logic_ok_tag = "欢迎您回来";


        //for mobile
        /*
        loginParams.put("username", Constants.SIS_USER);
        loginParams.put("password", Constants.SIS_PASSWORD);
        loginParams.put("questionid", "0");
        loginParams.put("answer", "");
        loginParams.put("cookietime", "315360000");
        loginParams.put("submit", "登 录");

        logic_ok_tag = "成功登录为";
        */

        Map<String, String> loginHeaders = new HashMap<String, String>(Constants.SIS_HTTP_HEADERS);
        loginHeaders.put("Referer", sisLoginURL);
        String response = DefaultConnectService.getInstance().doPost(sisLoginURL, loginHeaders, loginParams, "GBK");
        if (response.indexOf(logic_ok_tag) < 0) {
            throw new Exception();
        }
    }

    public static boolean isLogined() {
        List<Cookie> cookieList = DefaultConnectService.getInstance().getCookieStore().loadForRequest(HttpUrl.parse(Constants.SIS_URL));
        if (null != cookieList && !cookieList.isEmpty()) {
            for (Cookie cookie : cookieList) {
                if ("cdb3_auth".equals(cookie.name())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static List<TopicItem> fetchTopic(String forumURL, int pageNo) throws Exception {
        login();
        String url = String.format(forumURL, pageNo);
        url = String.format("%s%s", Constants.SIS_URL, url);
        if (Log.isLoggable(Constants.SIS_LOG_TAG, Log.DEBUG)) {
            Log.d(Constants.SIS_LOG_TAG, "fetchTopic " + url);
        }
        String response = DefaultConnectService.getInstance().doGet(url, Constants.SIS_HTTP_HEADERS);
        //Log.i(Constants.SIS_LOG_TAG,response);
        if (response.indexOf("您还没有登陆") > 0) {
            throw new Exception();
        }
        int prefixIndex = response.indexOf(Constants.TOPIC_LIST_PREFIX);
        if (prefixIndex < 0) {

            prefixIndex = response.indexOf(Constants.TOPIC_LIST_PREFIX2);
            if (prefixIndex < 0) {
                return null;
            }
            return null;
        }
        int suffixIndex = response.indexOf(Constants.TOPIC_LIST_SUFFIX, prefixIndex);
        if (suffixIndex < 0) {
            return null;
        }
        String content = response.substring(prefixIndex, suffixIndex);
        Matcher m = Constants.TOPIC_LIST_REGEX.matcher(content);
        List<TopicItem> items = new ArrayList<TopicItem>();
        while (m.find()) {
            String title = m.group(2).trim();
            if(!(title.endsWith("]")||title.endsWith("】"))){
                continue;
            }
            items.add(new TopicItem(title, m.group(1)));
        }
        if (Log.isLoggable(Constants.SIS_LOG_TAG, Log.DEBUG)) {
            Log.d(Constants.SIS_LOG_TAG, "fetchTopic " + url + " size=" + items.size());
        }
        return items;
    }

    public static String[] fetchTopicImages(String topicURL) throws Exception {

        login();
        String url = (topicURL.startsWith("http://")||topicURL.startsWith("https://")) ? topicURL : String.format("%s%s", Constants.SIS_URL, topicURL);
        if (Log.isLoggable(Constants.SIS_LOG_TAG, Log.DEBUG)) {
            Log.d(Constants.SIS_LOG_TAG, "fetchTopicImages " + url);
        }
        String response = DefaultConnectService.getInstance().doGet(url, Constants.SIS_HTTP_HEADERS);
        if (response.indexOf("您还没有登陆") > 0) {
            throw new Exception();
        }
        int prefixIndex = response.indexOf(Constants.TOPIC_IMAGE_PREFIX);
        if (prefixIndex < 0) {
            return null;
        }
        int suffixIndex = response.indexOf(Constants.TOPIC_IMAGE_SUFFIX, prefixIndex);
        if (suffixIndex < 0) {
            return null;
        }

        String content = response.substring(prefixIndex, suffixIndex);
        Matcher m = Constants.TOPIC_IMAGE_REGEX.matcher(content);
        List<String> items = new LinkedList<String>();
        while (m.find()) {
            String img = m.group(1);
            if (TextUtils.isEmpty(img)) {
                continue;
            }
            if (!(img.startsWith("http://") || img.startsWith("https://"))) {
                if(img.endsWith("/attachicons/image.gif") || img.endsWith("/attachimg.gif")){
                    continue;
                }
                img = String.format("%s%s", Constants.SIS_URL, img);
            }
            items.add(img);
        }
        return items.isEmpty() ? null : items.toArray(new String[0]);
    }

    public static String checkAvailableSISUrl(Context context) {
        for (String sisUrl : Constants.SIS_URLS) {
            String archiveUrl = Constants.getSISArchiveUrl(sisUrl);
            try {
                DefaultConnectService.getInstance().doGet(archiveUrl);
                Constants.SIS_URL = sisUrl;
                SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
                SharedPreferences.Editor prefsWriter = proxyPrefs.edit();
                prefsWriter.putString("SIS_URL",sisUrl);
                prefsWriter.commit();
                if(Log.isLoggable(Constants.SIS_LOG_TAG,Log.DEBUG)){
                    Log.d(Constants.SIS_LOG_TAG,sisUrl+" is available");
                }
                return sisUrl;
            } catch (Exception e) {
                Log.e(Constants.SIS_LOG_TAG,sisUrl+" not available",e);
            }
        }
        return null;
    }

    public static String recoverSISUrl(Context context){
        SharedPreferences proxyPrefs = context.getSharedPreferences(Constants.SIS_STORE_REF, 0);
        String url = proxyPrefs.getString("SIS_URL",null);
        if(null != url){
            Constants.SIS_URL = url;
        }
        return Constants.SIS_URL;
    }
}
