package net.vickymedia.sis.network;

import android.content.Context;

import com.nostra13.universalimageloader.core.assist.ContentLengthInputStream;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import net.vickymedia.net.ConnectService;
import net.vickymedia.net.ResponseHandler;


import java.io.IOException;
import java.io.InputStream;

import okhttp3.ResponseBody;

/**
 * Created by Ralph Jiang on 14-2-21.
 */
public class SISImageDownloader extends BaseImageDownloader{

    private ConnectService httpClient;

    public SISImageDownloader(Context context,ConnectService httpClient) {
        super(context);
        this.httpClient = httpClient;
    }

    @Override
    protected InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {
        try {
           return httpClient.doGet(imageUri, new ResponseHandler<ContentLengthInputStream>() {
                @Override
                public ContentLengthInputStream handleResponse(ResponseBody responseBody) throws IOException {
                    return new ContentLengthInputStream(responseBody.byteStream(),responseBody.contentLength());
                }
            });
        }catch (Exception ee){
            throw new IOException("fetch url:"+imageUri+" fail", ee);
        }
    }
}
