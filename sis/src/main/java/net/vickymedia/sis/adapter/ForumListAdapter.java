package net.vickymedia.sis.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.vickymedia.sis.R;
import net.vickymedia.sis.model.ForumItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ralph.jiang on 2/17/14.
 */
public class ForumListAdapter extends ArrayAdapter<ForumItem> {
    int layoutResourceId;
    public ForumListAdapter(Context context, int resource, ForumItem[] objects) {
        super(context, resource, new ArrayList<ForumItem>(Arrays.asList(objects)));
        this.layoutResourceId = resource;
    }

    public ForumListAdapter(Context context, int resource, List<ForumItem> objects) {
        super(context, resource, new ArrayList<ForumItem>(objects));
        this.layoutResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ForumItemHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ForumItemHolder();
            holder.imgIcon = (ImageView) row.findViewById(R.id.forumImgIcon);
            holder.txtTitle = (TextView) row.findViewById(R.id.forumTxtTitle);
            row.setTag(holder);
        } else {
            holder = (ForumItemHolder) row.getTag();
        }

        ForumItem forumItem = getItem(position);
        holder.txtTitle.setText(forumItem.getTitle());
        holder.imgIcon.setImageResource(forumItem.getIcon());

        return row;
    }

    public static class ForumItemHolder {
        ImageView imgIcon;
        TextView txtTitle;
    }

    public static ForumListAdapter build(Context context, int resource, TypedArray titles, TypedArray icons, TypedArray urls) {
        if (null == titles || titles.length() == 0) {
            return new ForumListAdapter(context, resource, new ForumItem[0]);
        }

        if (null == icons || icons.length() == 0 || titles.length() != icons.length() || titles.length() != urls.length()) {
            throw new RuntimeException("Title and icon array and urls length not equals.");
        }
        ForumItem items[] = new ForumItem[titles.length()];
        for (int i = 0; i < titles.length(); i++) {
            items[i] = new ForumItem(titles.getString(i), icons.getResourceId(i, 0));
            items[i].setUrl(urls.getString(i));
        }
        return new ForumListAdapter(context, resource, items);
    }
}
