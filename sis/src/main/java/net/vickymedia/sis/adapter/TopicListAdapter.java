package net.vickymedia.sis.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.vickymedia.sis.R;
import net.vickymedia.sis.model.TopicItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ralph.jiang on 2/17/14.
 */
public class TopicListAdapter extends ArrayAdapter<TopicItem> {

    int layoutResourceId;

    public TopicListAdapter(Context context, int layoutResourceId, TopicItem[] data) {
        super(context, layoutResourceId, new ArrayList<TopicItem>(Arrays.asList(data)));
        this.layoutResourceId = layoutResourceId;
    }

    public TopicListAdapter(Context context, int layoutResourceId, List<TopicItem> data) {
        super(context, layoutResourceId, new ArrayList<TopicItem>(data));
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TopicItemHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new TopicItemHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.topicTxtTitle);
            row.setTag(holder);
        } else {
            holder = (TopicItemHolder) row.getTag();
        }
        TopicItem topicItem = getItem(position);
        holder.txtTitle.setText(topicItem.getTitle());

        return row;
    }

    public static class TopicItemHolder {
        TextView txtTitle;
    }
}
