package net.vickymedia.sis;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Ralph Jiang on 2/17/14.
 */
public final class Constants {
    private Constants() {
    }

    public static final String SIS_LOG_TAG = "SIS";
    public static final String SIS_PHOTO_FOLDER = SIS_LOG_TAG;
    public static final String FORUM_ITEM_KEY = "FORUM_ITEM";
    public static final String TOPIC_ITEM_KEY = "TOPIC_ITEM";
    public static String SIS_URL = "http://oooo.1v2v3v4v.com/bbs/";
    public static String SIS_USER = "ralphjiang";
    public static String SIS_PASSWORD = "753951poiu";
    public static final List<String> SIS_URLS = new LinkedList<String>();
    public static final Map<String, String> SIS_HTTP_HEADERS = new HashMap<String, String>();
    //PC

    public static final String TOPIC_LIST_PREFIX = "<b>版块主题</b>";
    public static final String TOPIC_LIST_PREFIX2 = "<b>推荐主题</b>";
    public static final String TOPIC_LIST_SUFFIX = "<div class=\"pages_btns\">";
    public static final Pattern TOPIC_LIST_REGEX = Pattern.compile("<span [^>]+><a href=\"(thread-[^\"]+)\"[^>]*>([^<>]+)");

    /*public static final String TOPIC_LIST_PREFIX = "<table class='tlist'>";
    public static final String TOPIC_LIST_SUFFIX = "</table>";
    public static final Pattern TOPIC_LIST_REGEX = Pattern.compile("<a\\s+href=[\"']([^\"']+)[^>]+>([^<]+)");
    */

    //PC

    public static final String TOPIC_IMAGE_PREFIX = "<div id=\"postmessage_";
    public static final String TOPIC_IMAGE_SUFFIX = "<td class=\"postcontent\"";

    /*
    public static final String TOPIC_IMAGE_PREFIX = "<div class='page'";
    public static final String TOPIC_IMAGE_SUFFIX = "<div class=\"page\">";
    */
    public static final Pattern TOPIC_IMAGE_REGEX = Pattern.compile("<img\\s+[^<>]*src=[\"']([^'\"]+)");

    public static final String SIS_STORE_REF = "SISPrefsFile";

    static {
        SIS_HTTP_HEADERS.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0");
        SIS_HTTP_HEADERS.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        SIS_HTTP_HEADERS.put("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        SIS_URLS.add(SIS_URL);
    }

    public static void setSISHosts(String[] hosts) {
        if (null == hosts || hosts.length == 0) {
            return;
        }
        for (int i = 0; i < hosts.length; i++) {
            if (hosts[i] == null || hosts[i].trim().length() == 0) {
                continue;
            }
            String url = String.format("http://%s/bbs", hosts[i]);
            if (SIS_URLS.indexOf(url) < 0) {
                SIS_URLS.add(url);
            }
        }
    }

    public static String getSISLoginURL() {
        return String.format("%s%s", SIS_URL, "logging.php?action=login&");
        //return String.format("%s%s", SIS_URL, "simply/?action-index-mod-0.html");
    }

    public static String getSISArchiveUrl(String sisUrl){
        return String.format("%s%s", sisUrl, "archiver/");
    }

    public static class Config {
        public static final boolean DEVELOPER_MODE = false;
    }
}
