package net.vickymedia.net;

import java.util.List;
import java.util.Map;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.RequestBody;

/**
 * @author ralph.jiang
 */
public interface ConnectService {

    String doPost(String url) throws Exception;

    String doPost(String url, RequestBody entity) throws Exception;

    <T> T doPost(String url, RequestBody entity,
                 ResponseHandler<T> handler) throws Exception;

    String doPost(String url, Map<String, String> headers, RequestBody entity)
            throws Exception;

    <T> T doPost(String url, Map<String, String> headers, RequestBody entity,
                 ResponseHandler<T> handler) throws Exception;

    String doPost(String url, Map<String, String> headers,
                  Map<String, Object> params, String requestCharset) throws Exception;

    <T> T doPost(String url, Map<String, String> headers,
                 Map<String, Object> params, String paramString2,
                 ResponseHandler<T> handler) throws Exception;

    String doGet(String url) throws Exception;

    <T> T doGet(String url, ResponseHandler<T> handler) throws Exception;

    String doGet(String url, Map<String, String> headers) throws Exception;

    <T> T doGet(String url, Map<String, String> headers,
                ResponseHandler<T> handler) throws Exception;

    CookieJar getCookieStore();

    void shutDown();
}
