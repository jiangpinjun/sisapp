package net.vickymedia.net;

import android.database.CharArrayBuffer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.ResponseBody;

/**
 * Created by ralphjiang on 2017/5/9.
 */

public class AutoChartsetResponseHandler implements ResponseHandler<String> {


    public static ResponseHandler<String> INSTANCE = new AutoChartsetResponseHandler();

    private AutoChartsetResponseHandler(){}

    @Override
    public String handleResponse(ResponseBody responseBody) throws IOException {
        MediaType mediaType = responseBody.contentType();
        if(null == mediaType || mediaType.charset() == null){
            BufferedReader r = null;
            try {
                StringBuilder builder = new StringBuilder();
                r = new BufferedReader(new InputStreamReader(responseBody.byteStream(), "GBK"));
                String line = null;
                while ( (line = r.readLine()) != null ){
                    builder.append(line).append("\r\n");
                }
                return builder.toString();
            }finally {
                if(null != r){
                    try {
                        r.close();
                    }
                    catch (Exception e){

                    }
                }
            }
        }else{
            return responseBody.string();
        }
    }
}
