package net.vickymedia.net;

import android.text.TextUtils;

import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ralphjiang on 2017/5/9.
 */

public class DefaultConnectService implements ConnectService {

    private static DefaultConnectService INSTANCE = new DefaultConnectService();

    private int connectTimeout;
    private int readTimeout;

    private CookieJar cookieJar;

    public static DefaultConnectService getInstance(){
        return INSTANCE;
    }

    public DefaultConnectService(){
        this(5000,20000);
    }

    public DefaultConnectService(int connectTimeout,int readTimeout){
        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
    }

    @Override
    public String doPost(String url) throws Exception {
        return doPost(url,null);
    }

    @Override
    public String doPost(String url, RequestBody entity) throws Exception {
        return doPost(url,entity,AutoChartsetResponseHandler.INSTANCE);
    }

    @Override
    public <T> T doPost(String url, RequestBody entity, ResponseHandler<T> handler) throws Exception {
        return doPost(url, Collections.<String, String>emptyMap(),entity,handler);
    }

    @Override
    public String doPost(String url, Map<String, String> headers, RequestBody entity) throws Exception {
        return doPost(url,headers,entity,AutoChartsetResponseHandler.INSTANCE);
    }

    @Override
    public <T> T doPost(String url, Map<String, String> headers, RequestBody entity, ResponseHandler<T> handler) throws Exception {
        Request.Builder builder = new Request.Builder().url(url);
        if(null == entity){
            builder.post(RequestBody.create(null,new byte[0]));
        }else{
            builder.post(entity);
        }
        if(null != headers){
           for(Map.Entry<String,String> en : headers.entrySet()){
               builder.addHeader(en.getKey(),en.getValue());
           }
        }
        Request request = builder.build();
        Response response = okHttpClient().newCall(request).execute();
        return handler.handleResponse(response.body());
    }

    @Override
    public String doPost(String url, Map<String, String> headers, Map<String, Object> params, String requestCharset) throws Exception {
        return doPost(url,headers,params,requestCharset,AutoChartsetResponseHandler.INSTANCE);
    }

    @Override
    public <T> T doPost(String url, Map<String, String> headers, Map<String, Object> params, String requestCharset, ResponseHandler<T> handler) throws Exception {
        FormBody.Builder builder = new FormBody.Builder();
        if(null != params){
            String encoding = TextUtils.isEmpty(requestCharset) ? "UTF-8" : requestCharset;
            for(Map.Entry<String,Object> entry : params.entrySet()){
                if(null == entry.getValue()){
                    continue;
                }
                if(entry.getValue() instanceof Collection){
                    Collection c = (Collection)entry.getValue();
                    for(Object x : c){
                        if(null == x){
                            continue;
                        }
                        builder.addEncoded(entry.getKey(), URLEncoder.encode(x.toString(),encoding));
                    }
                }else if(entry.getValue().getClass().isArray()){
                    Object oo = entry.getValue();
                    int len = Array.getLength(oo);
                    for(int i=0;i<len;i++){
                        Object x = Array.get(oo,i);
                        if(null == x){
                            continue;
                        }
                        builder.addEncoded(entry.getKey(), URLEncoder.encode(x.toString(),encoding));
                    }

                }else{
                    builder.addEncoded(entry.getKey(), URLEncoder.encode(entry.getValue().toString(),encoding));
                }
            }
        }
        return doPost(url,headers,builder.build(),handler);
    }

    @Override
    public String doGet(String url) throws Exception {
        return doGet(url,Collections.<String, String>emptyMap());
    }

    @Override
    public <T> T doGet(String url, ResponseHandler<T> handler) throws Exception {
        return doGet(url,Collections.<String, String>emptyMap(),handler);
    }

    @Override
    public String doGet(String url, Map<String, String> headers) throws Exception {
        return doGet(url,headers,AutoChartsetResponseHandler.INSTANCE);
    }

    @Override
    public <T> T doGet(String url, Map<String, String> headers, ResponseHandler<T> handler) throws Exception {
        Request.Builder builder = new Request.Builder().url(url);
        if(null != headers){
            for (Map.Entry<String,String> en : headers.entrySet()){
                builder.addHeader(en.getKey(),en.getValue());
            }
        }
        Request request = builder.build();
        Response response = okHttpClient().newCall(request).execute();
        return handler.handleResponse(response.body());
    }

    @Override
    public CookieJar getCookieStore() {
        return cookieJar;
    }

    public void setCookieStore(CookieJar cookieStore){
        this.cookieJar = cookieStore;
    }

    protected OkHttpClient okHttpClient(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if(null!=cookieJar){
            builder.cookieJar(cookieJar);
        }
        if(connectTimeout > 0){
            builder.connectTimeout(connectTimeout, TimeUnit.MILLISECONDS);
        }
        if(readTimeout > 0) {
            builder.readTimeout(readTimeout,TimeUnit.MILLISECONDS);
        }
        return builder.build();
    }

    public void setHttpProxy(String proxy){

    }

    public void removeProxy(){

    }

    @Override
    public void shutDown() {

    }
}
