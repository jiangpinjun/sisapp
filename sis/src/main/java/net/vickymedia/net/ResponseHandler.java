package net.vickymedia.net;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import okhttp3.ResponseBody;

/**
 * @author ralph.jiang
 */
public interface ResponseHandler<T> {
    T handleResponse(ResponseBody responseBody) throws IOException;
}
